# Raspbell
---
## Installation-Manual

||||
|-|-|-|
|<b> Author: || Ingsoc DevTeam |
|<b> Date of Creation:|| 06.03.2022 |
|<b> Revision: ||  v0.1|
<br>

|<p style=text-align:left>Edits</p>|<p style=text-align:left>Name</p>|<p style=text-align:left>Date</p>|
|-|-|-|
|v0.1|F.Trautwein|06.03.2022|
### Dependencies

- Raspberry Pi
Any Generation above 3 will do. The system was developed on a RaspberryPi3A

- Raspbian 10 Buster
Found in the [Pi-Imager](https://www.raspberrypi.com/software/) as Raspberry Pi OS lite (Legacy).
> Choose OS -> Raspberry Pi OS (other) -> Raspberry Pi OS (Legacy)

### Introduction

This Guide assumes, that you already have flashed the Image of *Raspbian 10 Buster lite* on a Micro-SD, applied the *Hardwarereference* to your system and established a *SSH-Connection* to your Pi as User: *Pi*. Make sure the Pi has access to the Internet. Wifi is set up during the Installation. 
Please notice, that this installation does not cover every WiFi configuration!
If your WiFi requires more than a PSK-Authentication [this Guide](https://netbeez.net/blog/connect-your-raspberry-pi-to-wireless-enterprise-environments-with-wpa-supplicant/) may help. If you are not sure/don't know, don't bother.
It is recommended to connect the Pi directly via Ethernet to the Router.

This Guide is refering to the installationscript as [*install.sh*](https://gitlab.com/tim.goehringer/tuerklingel-12/-/blob/Entwurf_Weboberflaeche/install.sh) found in the GitLab repository.
You may browse to the users default directory by

`cd ~` or `cd /home/pi/`

### Step One
#### Method 1: copy the *install.sh*

- Download the *install.sh* from the repository to your device and transfer it to your Pi with:

`scp <location_of_install.sh> pi@<address_of_pi>:/home/pi/`
- Continue with Step Two

#### Method 2: Creating *install.sh*

- Create a Shell-Script and name it to your liking, but it has to end with '.sh'

`nano install.sh`
- Copy the contents of *install.sh* from the repository to your newly created Shell-Script
- Continue with Step Two

### Step Two

- Execute with bash...

`bash install.sh`
- Further Steps are discribed in the Pi-Terminal.
