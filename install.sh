#!/bin/sh

clear

echo "This is the installationscript for your doorbell."
echo "Please be sure that you followed the manual and met all requirements described in it."
echo
echo "This setup may take several minutes. You are able follow the proccess on screen."
echo "Start by pressing 'Enter'. Abort by pressing 'Ctrl+C'..."

read -r
rfkill unblock wlan
echo
echo "Do you wish to set up the WiFi? y/n"
read -r JAIN

if [ $JAIN = 'y' ]; then

echo "Enter the Name of your WiFi (SSID): "
read -r SSID
echo "Enter your WiFi-Password (PSK): "
read -r PSK
echo "Enter your Country-short in capitals (i.E.: DE): "
read -r CNT

sudo C=$CNT su root -c 'echo "country=$C" >> /etc/wpa_supplicant/wpa_supplicant.conf'
sudo S=$SSID P=$PSK su root -c 'wpa_passphrase $S $P >> /etc/wpa_supplicant/wpa_supplicant.conf'

sudo systemctl restart wpa_supplicant.service
fi

echo
echo "The Script is running on its own from now on. Grab some Coffee ;)"
sleep 1s
echo
echo "Performing system update..."

# Initial update
sudo apt update
sudo apt upgrade -y
sudo apt dist-upgrade -y

echo
echo "Installing required packages..."
echo
# Installing dependencies
sudo apt install -y curl npm
curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
sudo apt install -y ffmpeg mariadb-server mosquitto mosquitto-clients nodejs python3-pip git fswebcam

cd ~

echo
echo "Clonening repository from GitLab..."
echo
git clone https://gitlab.com/tim.goehringer/tuerklingel-12.git

cd ~/tuerklingel-12/ || exit

git checkout main
git pull

echo
echo "Setting up Python..."
echo
pip3 install --upgrade pip
pip3 install -r ~/tuerklingel-12/requirements.txt

cd src/web/

echo
echo "Setting up Node.js..."
echo 
npm install

cd ~/tuerklingel-12/

rPW=$(python src/bellMod/sysconfig.py rPw)
webPW=$(python src/bellMod/sysconfig.py webPw)
pyPW=$(python src/bellMod/sysconfig.py pyPw)

echo 
echo "Setting up MySQL..."
echo 

#DB setup
sudo mysql -u root --execute="CREATE DATABASE tuerklingel;"
sudo mysql -u root --execute="USE tuerklingel; SET time_zone = '+01:00';"
sudo mysql -u root --execute="USE tuerklingel; CREATE TABLE IF NOT EXISTS systemlog (ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, TimeStamp DATETIME DEFAULT NOW(), bell INT, contact INT, doorlock INT, doorunlock INT, Message VARCHAR(100));"
sudo mysql -u root --execute="USE tuerklingel; CREATE TABLE IF NOT EXISTS addresses (ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, address VARCHAR(50));"

# User setup
sudo mysql -u root --execute="CREATE USER 'web'@'localhost' IDENTIFIED BY '$webPW';"
sudo mysql -u root --execute="CREATE USER 'py'@'localhost' IDENTIFIED BY '$pyPW';"
sudo mysql -u root --execute="GRANT INSERT ON tuerklingel.systemlog TO py@localhost; GRANT SELECT(address) ON tuerklingel.addresses TO py@localhost;"
sudo mysql -u root --execute="GRANT SELECT, INSERT, DELETE ON tuerklingel.addresses TO web@localhost; GRANT SELECT ON tuerklingel.systemlog TO web@localhost;"

sudo mysql -u root --execute="ALTER USER 'root'@'localhost' IDENTIFIED BY '$rPW';"

echo
echo "Creating Services for autostart..."
echo

sudo cp ~/tuerklingel-12/src/service/*.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/bell*.service
sudo systemctl daemon-reload
sudo systemctl enable bellHard.service
sudo systemctl enable bellWeb.service
sudo systemctl start bellHard.service
sudo systemctl start bellWeb.service

# Finishing

echo "Installation complete."
echo "Press 'Enter' to reboot..."
read -r
sudo reboot