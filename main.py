import src.bellMod.Bell as Bell

#! Must be executed in repository-root ('tuerklingel-12/')


if __name__ == "__main__":
    """Working code which starts the doorbell"""

    bell = Bell.Bell()

    try:
        bell.log.info("Starting Application...")
        bell.start()

    except KeyboardInterrupt:
        bell.log.info("Interrupted... Terminating.")
        print("Interrupted... Terminating.")

    except Exception as e:
        bell.log.warning(f"{e}")
        print(e)

    finally:
        bell.log.info("Deleting bell...")
        del bell
