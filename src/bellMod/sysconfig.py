import json, sys

class sysconfig:
    """class to store values out of the sysConfig.json. Has Execution-Block to pass specific values to Shell.
    VAR=$(python <path>/sysconfig.py <arg>)
    Arguments are: | rPw | webPw | pyPw |"""

    ## Default Values

    # System
    sysMode = 1
    unlockTime = 5000 

    # Sounds
    soundPath = "res/sounds/"
    ringTone = "fogHorn.wav"
    accessTone = "elevatorDing.wav"

    # DB
    dbUser = 'root'     
    dbPw = '123456'
    dbHost = 'localhost'
    dbPort =  '3306'

    # Mail
    dev = "Ingsoc.Info@gmail.com"
    smtpServer = "smtp.gmail.com"
    mailPw = "4DihWnnsnshvfiF"
    mailTemplate = "src/mail/mail_template.html"
    mailCss = "src/mail/mail.css"
    pic = "res/img/screeny.jpg"
    logo = "res/img/logo.png"
    icon08 = "res/img/icons/icon08.png"
    mailson = "emailaddresses.json"

    # MQTT
    mqttHost = "127.0.0.1"
    mqttPublish = "bell/update"
    mqttRecieve = "bell/command"

    def __init__(self):
        """loads Values out of Json"""
        
        self.loadJson()



    def loadJson(self):
        """loads Jsonvalues into own Variables"""

        try:
            passData = json.loads(open("res/config/sysConfig.json", "r").read())
            self.sysMode = passData["sysMode"]                                 # initial value for sysMode, later be changed over Web-UI
            self.unlockTime = passData["unlockTime"]                           # in [ms]

            # Sounds
            self.soundPath = passData["soundPath"]
            self.ringTone = passData["ringTone"]
            self.accessTone = passData["accessTone"]

            ## DB
            self.rootDBUser = passData["dbUser"]
            self.rootDBPassword = passData["dbPw"]
            self.webDBUser = passData["webDBUser"]
            self.webDBPassword = passData["webDBPassword"]
            self.dbUser = passData["pyDBUser"]      # User for Hardware
            self.dbPw = passData["pyDBPassword"]    # Pw for Hardware
            self.dbHost = passData["dbHost"]
            self.dbPort =  passData["dbPort"]

            ## Mail
            self.dev = passData["dev"]
            self.smtpServer = passData["smtpServer"]
            self.mailPw = passData["mailPw"]
            self.mailTemplate = passData["mailTemplate"]
            self.mailCss = passData["mailCss"]
            self.pic = passData["pic"]
            self.logo = passData["logo"]
            self.icon08 = passData["icon08"]

            ## MQTT
            self.mqttHost = passData["mqttHost"]
            self.mqttPublish = passData["mqttUpdate"]
            self.mqttRecieve = passData["mqttCommand"]


        except KeyError as e:
            print("Key {} invalid".format(e))
            print("Using default values.")

        except FileNotFoundError as e:
            print(e)
            print("Using default values.")

        except:
            print("Failed to load json. Using default values.")

    
    def confUpdate(self):
        """loads Values from Json"""

        self.loadJson()


# Execution block for 'install.sh'. If executed with arguments in shell it prints corrissponding values.
# Used in Reference calls VAR=$(python <path>/sysconfig.py <arg>)
if __name__ == "__main__":

    conf = sysconfig()

    try:
        if sys.argv[1] == 'rPw':
            print(conf.rootDBPassword)

        elif sys.argv[1] == 'pyPw':
            print(conf.dbPw)

        elif sys.argv[1] == 'webPw':
            print(conf.webDBPassword)

    except:
        pass
