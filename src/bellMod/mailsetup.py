import smtplib, ssl
from email.mime.text import MIMEText            # Plain/HTML capable Mail
from email.mime.multipart import MIMEMultipart  # Massage-Structure
from email.mime.image import MIMEImage          # Image-Converter
import src.bellMod.sysconfig as sysConfig
import threading



class mailclass (threading.Thread):
    """this class handels the methods needed to send the Email towards the costumers, it inherits the the threading class"""

    
    def __init__(self, mails):
        """init initiates the class to make it"""

        self.__config = sysConfig.sysconfig
        threading.Thread.__init__(self)

        self.mails = mails
        
        
    def __addImg(self, pic, jpg_png, cid)->str:
        """add Img returns the image last taken form the camera"""

        imgData = open(pic, "rb").read()
        img = MIMEImage(imgData, jpg_png)
        img.add_header("Content-ID", cid)
        img.add_header("Content-Disposition", "inline", filename=cid)
        return img

    
    def __sendEmail(self):    
        """Sending E-Mails out of a container with a range of addresses"""

        for email in self.mails:        # looping to send each address within mails an email
            self.msg["To"]= email 

            with smtplib.SMTP_SSL(self.__config.smtpServer, 465, context=ssl.create_default_context()) as server:
                server.login(self.__config.dev, self.__config.mailPw)
                server.sendmail(self.__config.dev, email, self.msg.as_string())


    def run(self):
        """Building the Email"""

        self.msg = MIMEMultipart("alternative")        ## Massage-Structure            
        self.msg["Subject"] = "Jemand ist an der Tür!"        
        self.msg["From"]= self.__config.dev                        

        text = """\        
            Falls Bilder nicht richtig dargestellt werden, wird dies von Ihrer E-Mail-Platform oder Ihrem Webbrowser nicht unterstützt.

            Wechseln Sie gegebenenfalls zu einem (anderen) Webbrowser.
            """
        msgPart1 = MIMEText(text, "plain")              # Converting

        # HTML-Text: 
        # 'CSS-Injektion' -> split html-string at pos 33 (<!doctype html>...<head>|...</html>)
        html = open(self.__config.mailTemplate, "r").read()
        html = html[:33] + "<style>" + open(self.__config.mailCss, "r").read() + "</style>" + html[33:]
        msgPart2 = MIMEText(html, "html")               # Converting

        # Embeded-Images
        img01 = self.__addImg(self.__config.pic, "jpeg", "<image01>")
        img02 = self.__addImg(self.__config.logo, "png", "<logo>")
        img03 = self.__addImg(self.__config.icon08, "png", "<icon08>")

        # Combining all strings
        self.msg.attach(msgPart1)                            # Plain-Text
        self.msg.attach(msgPart2)                            # HTML
        self.msg.attach(img01)                               # Camera-Snapshot
        self.msg.attach(img02)                               # Logo
        self.msg.attach(img03)                               # Icon08
    
        # Send E-Mails   
        self.__sendEmail()