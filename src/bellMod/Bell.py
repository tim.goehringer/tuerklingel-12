import logging, time, random, subprocess, threading
import paho.mqtt.client as mqttlib
import mysql.connector as mariadb
import src.bellMod.mailsetup as mailsetup
import src.bellMod.sysconfig as sysConfig
import RPi.GPIO as GPIO
from os import system, listdir


class Bell():
    """\
    Description: The Bell-Class creates the main Logic on a Raspberry Pi running Respbian10(buster).
    It depends on a MariaDB-Connection. It is able to send an e-mail to multible addresses.
    """

    __logMsg = {
        "0": 'The System was in OFF-Mode',
        "1": 'Bellbutton was pressed',
        "2.1": 'Door manually unlocked',
        "2.2": 'Door automaticly unlocked',
        "2.3": 'Door was not unlocked in time',
        "3.1": 'Door was opened',
        "3.2": 'Door was closed',
        "3.3": 'Door was not opened in time',
        "4": 'Visit successful'
    }

    # GPIO-Pins
    __lock = 26
    __lockStatus = 5
    __contact = 23
    __unlock = 13
    __bell = 24

    def __init__(self):     # Constructor

        self.gpioSetup()

        self.__config = sysConfig.sysconfig()
        self.__config.loadJson()

        self.__setupLog()
        self.__dbConection()

        self.__mqttclient = mqttlib.Client()
        self.__mqttclient.on_connect = self.mqttOnConnect
        self.__mqttclient.message_callback_add(self.__config.mqttRecieve, self.onMessageFromUI)

        self.randomRange = listdir(self.__config.soundPath)

        # 'Controller'-Booleans
        self.__connected = False        # Enableing 'start()'-while-loop
        self.__webUIunlock = [False]    # Value to handle MQTT-Message 'Unlock'. Must be listed, due to use in '__timeOutFalse'
        self.__listen = False           # Workaround
        self.__randomTone = False       #

        # Pointer-Trick for Python. Using Mutable objects to pass by reference.
        self.__bBell = [False]
        self.__bContact = [False]
        self.__bUnlock = [False]
        self.__bLockStatus = [False]

        self.connectToMqtt()

    def __del__(self):  # Destructor
        try:
            self.__mqttclient.disconnect()
            del self.__mqttclient
            del self.mailThread

        except AttributeError:
            pass

        del self.__connected
        del self.__webUIunlock
        del self.log
        del self.__config
        
        GPIO.cleanup()

        system("sudo systemctl stop bellHard.service")
        system("sudo systemctl stop bellWeb.service")


    def start(self):
        """Must only be started once for each Bell-Class. Starting Main-Loop. Breaks on invalid sysMode."""

        while True:     # sysMode: Off=0, Man=1, Auto=2
            
            if self.__connected:        # only run code if connected
                
                conFail = 0
                if self.__webUIunlock[0]:      # Loop control: wait_for_edge conflict when WebUI unlocks door out of regular process

                    time.sleep(.5)
                    continue
                
                # self.log.info("System Cycled (1 sec interval).")        # Notification to detect softlocks
                if self.__timeOutTrue(self.__bBell):

                    self.__listen = True        # Preventing multible __grantAccess executions, denying Web-UI unlock during regular process

                    self.log.info(self.__logMsg["1"])
                    self.__dbLog(self.__logMsg["1"])
                    self.__mqttclient.publish(self.__config.mqttPublish, "Ring")

                    if self.__config.sysMode == 1:      # Manual

                        system(f"fswebcam -q -r 520x520 {self.__config.pic} -p YUYV")
                        self.__mqttclient.publish(self.__config.mqttPublish, "Picture")

                        #starting new thread which plays sound
                        self.playRingTone()
                        self.__bBell[0] = False

                        #starting new thread which sends Mail
                        self.mailThread = mailsetup.mailclass(self.__getMails())
                        self.mailThread.daemon = True
                        self.log.info("Starting Mailthread")
                        self.mailThread.start()


                        if self.__timeOutTrue(self.__bUnlock, timeout=self.__config.unlockTime) or self.__timeOutTrue(self.__webUIunlock, timeout=self.__config.unlockTime):      # waiting for unlock, either button or web
                            
                            if self.__webUIunlock[0]:      # Web unlock handler
                                self.log.info(self.__logMsg["2.1"] + " with Web-UI")
                                self.__dbLog(self.__logMsg["2.1"] + " with Web-UI")
                            
                            else:       # hardware handler
                                self.log.info(self.__logMsg["2.1"])
                                self.__dbLog(self.__logMsg["2.1"])

                            self.__webUIunlock[0] = False      # reset Boolian to prevent softlock
                            self.__bUnlock[0] = False

                            self.__grantAccess()
                            # self.__listen = False       # Enabeling UI to manually unlock Door, reguardless of 'def start()'
                            

                        else:
                            self.log.info(self.__logMsg["2.3"])
                            self.__dbLog(self.__logMsg["2.3"])
                            # self.__listen = False


                    elif self.__config.sysMode == 2:        # Auto

                        system(f"fswebcam -q -r 520x520 {self.__config.pic} -p YUYV")
                        self.__mqttclient.publish(self.__config.mqttPublish, "Picture")

                        self.mailThread = mailsetup.mailclass(self.__getMails())
                        self.mailThread.daemon = True
                        self.mailThread.start()     #starting new thread which sends Mail
                        self.log.info("Starting Mailthread")

                        self.log.info(self.__logMsg["2.2"])
                        self.__dbLog(self.__logMsg["2.2"])

                        self.__grantAccess()
                        # self.__listen = False

                    elif self.__config.sysMode == 0:        # Off

                        self.log.info(self.__logMsg["0"])
                        self.__dbLog(self.__logMsg["0"])
                        # self.__listen = False

                    else:                                                 # Exeption
                        
                        self.log.warning(f"Unexepted Systemmode: {self.__config.sysMode} Type: {type(self.__config.sysMode)}! Terminating")
                        self.__dbLog(f"Unexepted Systemmode: {self.__config.sysMode} Type: {type(self.__config.sysMode)}! Terminating")
                        self.__mqttclient.publish(f"Unexepted Systemmode: {self.__config.sysMode}!\n 'INT' required, got {type(self.__config.sysMode)}")
                        break

                    self.__listen = False

                    # waiting for threads to finish
                    if self.__config.sysMode != 0:
                        
                        self.mailThread.join()
                        self.log.info("Mailthread joined")
                        del self.mailThread

            else:

                try:

                    self.connectToMqtt()
                    
                except:

                    conFail += 1

                    if conFail >= 3:

                        self.log.warning(f"Failed to reconnect to MQTT {self.__config.mqttHost}")
                        break


            self.__webUIunlock = [False]
            self.__bBell = [False]
            self.__bUnlock = [False]
            self.__bLockStatus = [False]

   
    ## Logger
    def __setupLog(self):     # Storage for log Variables
        """Setting up a basic logger with terminal output only."""

        self.log = logging.getLogger('IngLog')
        self.log.setLevel(logging.DEBUG)

        ch = logging.StreamHandler()    # Console output
        ch.setLevel(logging.DEBUG)

        form = logging.Formatter(
        '[%(asctime)s] %(name)-15s %(levelname)-12s - %(message)s'
        )

        ch.setFormatter(form)
        self.log.addHandler(ch)

        self.log.info("Consoleoutput for logs established.")


    ## DB
    # Connect
    def __dbConection(self):
        """Connecting Bell-Class using mariadb-connect-rf"""

        try:    #logging in
            self.mariadbConnection = mariadb.connect(
                user=self.__config.dbUser,
                password=self.__config.dbPw,
                host=self.__config.dbHost,
                port=self.__config.dbPort
                )
            self.dbCursor = self.mariadbConnection.cursor()

        except:
            self.log.warning("Could not establish connection! Events cannot be logged!")

        else:
            self.dbCursor.execute("USE tuerklingel")
            self.log.info(f"Connected to DB as {self.__config.dbUser}.")
        

    # logging into DB
    def __dbLog(self, msg: str):

        """Logs the current Pin-Values with a custom Message"""

        values = (
            GPIO.input(self.__bell),
            GPIO.input(self.__contact),
            GPIO.input(self.__lockStatus),
            GPIO.input(self.__unlock),
            msg
            )
        
        try:    
            self.dbCursor.execute("INSERT INTO systemlog (bell, contact, doorlock, doorunlock, Message) VALUES ( %s, %s, %s, %s, %s)", values)
            self.mariadbConnection.commit()

        except:
            self.__mqttclient.publish(self.__config.mqttPublish, "Logging failed!")
            self.log.error("Could not insert into table!")
            self.mariadbConnection.rollback()
            self.log.info("Reconnecting...")
            del self.mariadbConnection
            del self.dbCursor
            self.__dbConection()


    def __realTimeStat(self, channel):
        """Callback-Function on rising or falling edge-detection. Passes name and value of Input-Channel over MQTT to Websocket"""

        if channel == self.__bell:

            chanName = "bell"

            if GPIO.input(channel) == 1:
                
                self.__bBell[0] = True

            elif GPIO.input(channel) == 0:

                self.__bBell[0] = False

        elif channel == self.__contact:

            chanName = "contact"

            if GPIO.input(channel) == 1:

                self.log.info(self.__logMsg["3.1"])
                self.__dbLog(self.__logMsg["3.1"])
                self.__bContact[0] = True

            elif GPIO.input(channel) == 0:

                self.log.info("Door closed")
                self.__dbLog("Door closed")
                GPIO.output(self.__lock, GPIO.LOW)
                self.__bContact[0] = False

        elif channel == self.__unlock:

            chanName = "unlock"

            if GPIO.input(channel) == 1:

                self.__bUnlock[0] = True

            elif GPIO.input(channel) == 0:

                self.__bUnlock[0] = False

        elif channel == self.__lockStatus:

            chanName = "lock"

            if GPIO.input(channel) == 1:

                self.__bLockStatus[0] = True

            elif GPIO.input(channel) == 0:

                self.__bLockStatus[0] = False


        self.__mqttclient.publish(self.__config.mqttPublish, f"Update,{chanName},{GPIO.input(channel)}")


    # retrieving mails out of DB
    def __getMails(self):
        """Collecting mariadb-cursor outputs into an empty list and returns it."""
        addresses = []

        self.dbCursor.execute("SELECT address FROM addresses")
        
        for address in self.dbCursor:

            addresses.append(address[0])
            
        self.log.info(f"'getMails' retreived {str(len(addresses))} addresses.")


        return addresses


    ## Bell funktion
    # Opening the Door
    def __grantAccess(self):
        """Responseble for disengaging the Lock and playing the Greetingsound. Notices Pin values and corrisponding representivly."""

        GPIO.output(self.__lock, GPIO.HIGH)    #opening lock
        # self.__mqttclient.publish(self.__config.mqttPublish, f"Update,lock,{GPIO.input(self.__lockStatus)}")
        self.playAccessTone()

        if self.__timeOutTrue(self.__bContact, timeout=5000):       # waiting for Door to open

            pass

        elif GPIO.input(self.__contact) == 1:

            self.log.warning("Door still open!")
            self.__dbLog("Web-UI unlocked Door, but Door is still open.")
        
        else:

            self.log.info(self.__logMsg["3.3"])
            self.__dbLog(self.__logMsg["3.3"])
            GPIO.output(self.__lock, GPIO.LOW)
            self.__mqttclient.publish(self.__config.mqttPublish, f"Update,lock,{GPIO.input(self.__lockStatus)}")


        if not self.__timeOutFalse(self.__bContact, timeout=10000):     # Pending until Button-Release/Door closed

            # self.__mqttclient.publish(self.__config.mqttPublish, f"Update,lock,{GPIO.input(self.__lockStatus)}")
            self.log.info(self.__logMsg["4"])
            self.__dbLog(self.__logMsg["4"])

        elif GPIO.input(self.__contact) == 1:       # timeout handler

            self.log.info("Door left open!")
            self.__dbLog("Door left open!")
        

    def gpioSetup(self):
        """Setting up the Pins of the Pi"""

        # Basic GPIO config
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)

        # Pin Setup
        GPIO.setup(self.__lock, GPIO.OUT)                                   # LOCK
        GPIO.setup(self.__lockStatus, GPIO.IN)                              # LOCK-Output Collector
        GPIO.setup(self.__contact,GPIO.IN, pull_up_down= GPIO.PUD_DOWN)     # DOORCONTACT
        GPIO.setup(self.__unlock,GPIO.IN, pull_up_down= GPIO.PUD_DOWN)      # UNLOCKBUTTON
        GPIO.setup(self.__bell, GPIO.IN, pull_up_down= GPIO.PUD_DOWN)       # BELLBUTTON

        # Eventdetection
        GPIO.add_event_detect(self.__bell, GPIO.BOTH, bouncetime=5, callback=self.__realTimeStat)
        GPIO.add_event_detect(self.__contact, GPIO.BOTH, bouncetime=5, callback=self.__realTimeStat)
        GPIO.add_event_detect(self.__lockStatus, GPIO.BOTH, bouncetime=5, callback=self.__realTimeStat)
        GPIO.add_event_detect(self.__unlock, GPIO.BOTH, bouncetime=5, callback=self.__realTimeStat)

    
    def playRingTone (self):        # thread-function for Ringtone
        """Funktion to play sounds in a subprocess"""

        subprocess.call(["aplay", f"{self.__config.soundPath}{self.__config.ringTone}"])

    def playAccessTone (self):      # thread-function for Accesstone
        """Funktion to play sounds in a subprocess"""

        if self.__randomTone:       # random config handler

            random.shuffle(self.randomRange)
            self.__config.accessTone = self.randomRange[0]


        subprocess.call(["aplay", f"{self.__config.soundPath}{self.__config.accessTone}"])

    def __timeOutTrue(self, checker: list, timeout=1000) -> bool:        # function using lists to pass by reference
        """Counts up to given miliseconds and returns 'False' if the checker stays 'False', else 'True'.\n Default looptime is 1 second"""

        timeoutCount = 0

        while checker[0] is False:

            timeoutCount += 1
            time.sleep(0.001)

            if timeoutCount >= timeout:

                return False                # Returnvalue if timout exceeded

        return True                         # Returnvalue if checker changed

    def __timeOutFalse(self, checker: list, timeout=1000) -> bool:      # function using lists to pass by reference
        """Counts up to given seconds and returns 'True' if the checker stays 'True', else 'False'.\n Default looptime is 1 second"""

        timeoutCount = 0

        while checker[0] is True:

            timeoutCount += 1
            time.sleep(0.001)

            if timeoutCount >= timeout:

                return True                 # Returnvalue if timout exceeded

        return False                        # Returnvalue if checker changed
    
    ## MQTT
    def connectToMqtt(self):
        """Connect application with the mqtt-server"""
        try:

            self.__mqttclient.connect(self.__config.mqttHost)

        except ConnectionError as e:

            self.log.error("No connection could be established.")
            return False

        else:       # Start MQTT Client
            
            self.__mqttclient.loop_start()
            self.__connected = True
            self.log.info(f"Successfully connected to '{self.__config.mqttHost}'.")


    def onMessageFromUI(self, client, userdata, message: mqttlib.MQTTMessage):

        # Convert MQTT-Content to string
        payload = str(message.payload.decode())
        self.log.info(f'Message from Webserver: {payload}')

        # Check for massage
        if payload == "unlock":

            self.log.info("Door unlocked from Webserver")
            self.__dbLog("Door unlocked from Webserver")
            self.__webUIunlock[0] = True
            self.__bUnlock[0] = True

            if not self.__listen:

                self.__mqttclient.publish(self.__config.mqttPublish, "Update,lock,1")
                self.log.debug("Commencing webThread...")
                webThread = threading.Thread(target= self.__grantAccess)
                webThread.start()
                webThread.join()
                self.log.info("Webthread finished. Deleting")
                del webThread
                # self.__grantAccess()
            

        elif payload == "Off":        # Changing sysMode

            self.__config.sysMode = 0

            # Event-Logging
            self.log.info(f"sysMode changed to {payload}")
            self.__dbLog(f"sysMode changed to {payload}")

        elif payload == "Man":        # Changing sysMode

            self.__config.sysMode = 1

            # Event-Logging
            self.log.info(f"sysMode changed to {payload}")
            self.__dbLog(f"sysMode changed to {payload}")

        elif payload == "Auto":       # Changing sysMode

            self.__config.sysMode = 2

            # Event-Logging
            self.log.info(f"sysMode changed to {payload}")
            self.__dbLog(f"sysMode changed to {payload}")
        
        elif payload == "Random":     # enabling random sound

            self.randomRange = listdir(self.__config.soundPath)
            self.__randomTone = True

            # Event-Logging
            self.log.info("Greetingsounds are now selected randomly")
            self.__dbLog("Greetingsounds are now selected randomly")

        elif payload == "Time":

            self.__config.loadJson()
            self.log.info(f"Time to unlock set to {self.__config.unlockTime} milliseconds")
            self.__dbLog(f"Time to unlock set to {self.__config.unlockTime} milliseconds")

        elif payload.__contains__(".wav"):       # Changing to specific sound

            self.__randomTone = False
            self.__config.accessTone = payload

            # Event-Logging
            self.log.info(f"Greetingsound set to '{payload}'")
            self.__dbLog(f"Greetingsound set to '{payload}'")


    def mqttOnConnect(self, client, userdata, flags, rc):
        
        self.log.info('MQTT Client connection established')

        # Listen to Subscription
        try:

            client.subscribe(self.__config.mqttRecieve)

        except:

            self.log.error("Wrong or invalid MQTT HEAD.")

        else:

            self.log.info(f"Subscription to {self.__config.mqttRecieve} successfull.")

