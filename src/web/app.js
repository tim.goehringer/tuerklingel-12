const express = require("express");
const http = require("http");
const WebSocket = require("ws");
const mqtt = require("mqtt");
const fs = require("fs");
const bp = require("body-parser");
const mysql = require("mysql");
const fileUpload = require("express-fileupload");
const path = require("path");
const multer = require("multer");
const configFilename = "../../res/config/sysConfig.json";
const sysConfig = require(configFilename);
const ffmpeg = require("fluent-ffmpeg");

const webUIHost = sysConfig.webAddress;
const webUIPort = sysConfig.webPort;
const webSocketPort = sysConfig.webSocketPort;
const cssFolderName = "css";
const htmlFolderName = "html";
const imgFolderName = "../../res/img";
const jsFolderName = "js";
const jsonFolderName = "json";
const soundFolder = "../../res/sounds/";
const mqttUrl = "mqtt://" + sysConfig.mqttHost;
const mqttReceiveTopic = sysConfig.mqttUpdate;
const mqttSendTopic = sysConfig.mqttCommand;

const mqttClient = mqtt.connect(mqttUrl);
const app = express();
const webServer = http.createServer(app);
const webSocketServer = new WebSocket.Server({ server: webServer });

const DBConnection = mysql.createConnection({
  host: sysConfig.dbHost,
  user: sysConfig.webDBUser,
  password: sysConfig.webDBPassword,
  database: sysConfig.dbName,
});

var sysStatus = {
  bell: 0,
  unlock: 0,
  contact: 0,
  lock: 0,
};

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, soundFolder);
  },
  filename: function (req, file, cb) {
    cb(null, "tempSound");
  },
});

const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (file.mimetype != "audio/wav") {
      cb(null, false);
    } else {
      cb(null, true);
    }
  },
}).single("soundUpload");

//Checks if the Address submitted by the user already exists in the database.
function alreadyExistant(testaddress, callback) {
  var sql =
    'SELECT EXISTS (SELECT 1 FROM addresses WHERE address = ?) AS "check"';
  DBConnection.query(sql, testaddress, (err, res) => {
    if (err) throw err;
    return callback(JSON.parse(JSON.stringify(res))[0].check);
  });
}

// Will delete an address from the database.
removeAddresses = (arr, callback) => {
  var addressArr = [];
  arr.forEach((address) => {
    addressArr.push(address.address);
  });
  var queryArr = [addressArr];
  var sql = "DELETE FROM addresses WHERE address IN (?)";
  DBConnection.query(sql, queryArr, (err, result) => {
    if (err) throw err;
    return callback(JSON.parse(JSON.stringify(result)));
  });
};

initMqttClient = () => {
  /**
   * Function to execute on successful connection with the mqtt server.
   */
  mqttClient.on("connect", () => {
    console.log(`Successfully connected MQTT. Address: ${mqttUrl}`);

    // Abonniere ausgewähltes Thema
    mqttClientSubscribeToTopic(mqttReceiveTopic);
  });

  /**
   * Function to be executed on new MQTT message in the receiving topic.
   */
  mqttClient.on("message", (topic, message) =>
    handleMqttMessage(topic, message)
  );
};

/**
 * Subscribe to a MQTT topic
 *
 * @param topic Topic to be subscribed to.
 */
mqttClientSubscribeToTopic = (topic) => {
  mqttClient.subscribe(topic, {}, (error, granted) => {
    if (granted !== null) {
      console.log(
        `Subscription succesfully created. Topic = ${mqttReceiveTopic}`
      );
    } else {
      console.error(error);
    }
  });
};

/**
 * Handle incoming MQTT messages
 *
 * @param topic Topic of the incoming message
 * @param message Message Content
 */
//! Sometimes undefined gets written to object, not consistently reproducible
handleMqttMessage = (topic, message) => {
  var strMessage = message.toString().trim();
  console.log("MQTT message received: ", strMessage);
  if (message == "Ring" || message == "Picture") {
    sendMessageToAllClients(strMessage);
  } else {
    var messageArr = strMessage.split(",");
    if (messageArr[2] !== 0 || messageArr[2] !== 1) {
      console.log(messageArr);
      sysStatus[messageArr[1]] = messageArr[2];
      sendMessageToAllClients(messageArr[0]);
    }
  }
};

/**
 * Send a message to all connected clients.
 *
 * @param message Message to be sent.
 */
sendMessageToAllClients = (message) => {
  console.log("Sending message to clients: " + message);
  webSocketServer.clients.forEach((client) => {
    client.send(message);
  });
};

initWebServer = () => {
  // Setup the static directories
  app.use("/css", express.static(cssFolderName));
  app.use(express.static(htmlFolderName));
  app.use("/js", express.static(jsFolderName));
  app.use("/json", express.static(jsonFolderName));
  app.use("/img", express.static(imgFolderName));

  // Tell the server to use one of the following Parsers for HTTP Requests
  app.use(bp.json());
  app.use(bp.urlencoded({ extended: true }));
  app.use(bp.text());
  var jsonParser = bp.json();

  /**
   * * INDEX
   */

  // Executed when the OpenDoor button on the UI is pressed.
  // Will send a MQTT message to the python script.
  app.post("/openDoor", (req, res) => {
    mqttClient.publish(mqttSendTopic, "unlock", function (err) {
      if (err) {
        console.error(err);
        res.send("MQTT Error").status(500);
      }
    });
    console.log("Door opened");
    res.sendStatus(200);
  });

  // Will send the current Picture, recorded by the camera, to the client.
  app.get("/cameraImg", (req, res) => {
    //Deactivate caching to not fill up client cache
    res.set("cache-control", "no-cache");
    res.sendFile(path.resolve("../../res/img/screeny.jpg"));
  });

  // Executed when the Systemmode on the UI is changed.
  // Will write the selected mode to the sysConfig.json file.
  // Will send the selected mode to the python script.
  app.post("/changeMode", jsonParser, (req, res) => {
    sysConfig.sysMode = parseInt(req.body.Mode);
    fs.writeFile(
      configFilename,
      JSON.stringify(sysConfig, null, 2),
      function writeJSON(err) {
        if (err) throw err;
      }
    );
    switch (req.body.Mode) {
      case "0":
        mqttClient.publish(mqttSendTopic, "Off", function (err) {
          if (err) {
            console.error(err);
            res.send("MQTT Error").status(500);
          }
        });
        break;
      case "1":
        mqttClient.publish(mqttSendTopic, "Man", function (err) {
          if (err) {
            console.error(err);
            res.send("MQTT Error").status(500);
          }
        });
        break;
      case "2":
        mqttClient.publish(mqttSendTopic, "Auto", function (err) {
          if (err) {
            console.error(err);
            res.send("MQTT Error").status(500);
          }
        });
        break;
    }
    res.send("Succesfully sent MQTT Message").status(200);
  });

  // Will get the current system mode from the config File and return it to the client.
  app.get("/getSysMode", (req, res) => {
    res.send(sysConfig.sysMode.toString()).status(200);
  });

  // Gets the current Status of the system from the database.
  // Sends the response back to the client.
  app.get("/getSysStatus", (req, res) => {
    var sysArray = [];
    for (const [key, value] of Object.entries(sysStatus)) {
      sysArray.unshift(JSON.parse(`{"Input": "${key}", "State": "${value}"}`));
    }
    res.send(sysArray);
  });
  //History
  app.get("/getHistory", (req, res) => {
    var sql =
      "SELECT DATE_FORMAT(TimeStamp, '%d.%m.%Y') AS Date, DATE_FORMAT(TimeStamp, '%H:%i') AS Time FROM systemlog WHERE bell = 1  ORDER BY TimeStamp DESC LIMIT 5 ";
    DBConnection.query(sql, (err, results) => {
      res.send(JSON.parse(JSON.stringify(results))).status(200);
    });
  });

  /**
   * * HISTORY
   */

  // Will get all current addresses from the database.
  // Send the results back to the client.
  app.get("/getAddresses", (req, res) => {
    DBConnection.query(
      "SELECT `address` FROM `addresses`",
      function (err, results) {
        if (err) throw err;
        res.send(JSON.parse(JSON.stringify(results))).status(200);
      }
    );
  });

  /**
   * * SETTING
   */

  // * Addresses

  // Executed when user submits a new email address.
  // Will again check the validity of the email.
  // If valid, will store the email in the database.
  app.post("/addAddress", jsonParser, (req, res) => {
    var writeData = req.body;
    if (
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        JSON.stringify(writeData).address
      )
    ) {
      console.log("Faulty Input");
      res.status(409).send("Faulty Input");
    } else {
      alreadyExistant(writeData.address, (result) => {
        if (result) res.status(409).send("Address already exists");
        else {
          var sql = "INSERT INTO `addresses` (address) VALUES (?)";
          DBConnection.query(sql, writeData.address, function (err) {
            if (err) throw err;
            res.status(201).send("Address successfully added!");
          });
        }
      });
    }
  });

  // Called when the user deletes an address.
  app.post("/removeAddress", jsonParser, (req, res) => {
    removeAddresses(req.body, (result) => {
      res.send(`Deleted ${result.affectedRows} addresses.`).status(200);
    });
  });

  // * Sounds

  // Returns the current list of sounds in the corresponding folder
  app.get("/soundList", (req, res) => {
    var fileList = [];
    var object = {};
    fileList.push({ fileName: "Random" });
    fs.readdir(soundFolder, (err, files) => {
      if (err) {
        console.error(err);
        res.send(err).status(500);
      }
      files.forEach((file) => {
        object.fileName = file;
        fileList.push(object);
        fileList = JSON.parse(JSON.stringify(fileList));
      });
      res.send(fileList).status(200);
    });
  });

  // Returns the currently selected sound to the client.
  app.get("/getActiveSound", (req, res) => {
    res.send(sysConfig.accessTone).status(200);
  });

  // Tell Express to use the file handler for the post request
  //app.use(fileUpload());

  // Will check the user-uploaded file for the correct filetype
  // If the file is valid save it in the /sounds/ directory
  app.post("/newSound", (req, res) => {
    upload(req, res, function (err) {
      if (err) {
        console.log(err);
      }
      //Necessary conversion from wav to wav to make file compatible with aplay
      ffmpeg(soundFolder + "tempSound")
        .toFormat("wav")
        .on("error", (err) => {
          console.log(err.message);
          res.send("An error occurred: " + err.message);
        })
        .on("end", () => {
          console.log("Processing finished !");
          fs.unlink(soundFolder + "tempSound", (err) => {
            console.log(err);
          });
          res.send("File uploaded!");
        })
        .save(soundFolder + req.file.originalname + ".wav");
    });
  });

  // Called when the user deletes a sound on the UI
  // Will delete the corresponding file
  app.post("/removeSound", jsonParser, (req, res) => {
    var fileName = req.body[0].fileName;
    if (fileName == "Random") {
      res.send("Cannot delete Random sound").status(409);
      return;
    } else {
      var filePath = __dirname + "/../../res/sounds/" + fileName;
      fs.unlink(filePath, (err) => {
        if (err) {
          console.error(err);
          res.send("Couldn't delete sound").status(500);
        }
      });
      res.send("Successfully deleted sound: " + fileName).status(200);
    }
  });

  // Called when the user selects a new sound on the UI.
  // Will send the selected soundname to the python script and
  // writes the soundname to the config file
  app.post("/setSound", jsonParser, (req, res) => {
    mqttClient.publish(mqttSendTopic, req.body.fileName, function (err) {
      if (err) {
        console.error(err);
        res.send("MQTT Error").status(500);
      }
    });
    sysConfig.accessTone = req.body.fileName;
    fs.writeFile(
      configFilename,
      JSON.stringify(sysConfig, null, 2),
      function writeJSON(err) {
        if (err) throw err;
      }
    );
    res.send("Successfully changed sound to: " + req.body.fileName).status(200);
  });

  // * Unlock time

  // Will get the current unlock time from the config file
  // And return it to the client
  app.get("/getUnlockTime", (req, res) => {
    var userTime = (parseInt(sysConfig.unlockTime) / 1000).toString();
    res.send(userTime).status(200);
  });

  // Called when the user submits a new Unlock time
  // Will write the selected time into the config file
  app.post("/setUnlockTime", (req, res) => {
    if (isNaN(req.body)) res.send("Invalid Input").status(409);
    sysConfig.unlockTime = parseInt(req.body) * 1000;
    fs.writeFile(
      configFilename,
      JSON.stringify(sysConfig, null, 2),
      function writeJSON(err) {
        if (err) {
          console.error(err);
          res.send("Couldn't set unlock time").status(500);
        } else {
          mqttClient.publish(mqttSendTopic, "Time", function (err) {
            if (err) {
              console.error(err);
            }
          });
          res
            .send(
              "Successfully changed the unlock time to " +
                req.body.fileName +
                " seconds"
            )
            .status(200);
        }
      }
    );
  });
};

startWebServer = () => {
  // Starte WebServer unter ausgewähltem Port
  app.listen(webUIPort, webUIHost, () => {
    console.log(`Started WebServer. Address: http://${webUIHost}:${webUIPort}`);
  });
};

initWebsocketServer = () => {
  /**
   * Function, to be executed on successfull connection
   */
  webSocketServer.on("connection", (ws) => {
    // Connection successfull, send client feedback.
    ws.send("Verbindung erfolgreich.");
  });
};

/**
 * Start Websocket Server
 */
startWebSocketServer = () => {
  webServer.listen(webSocketPort, webUIHost, () => {
    console.log(
      `Started WebSocketServer. Address: http://${webUIHost}:${webSocketPort}`
    );
  });
};

startDBConnection = () => {
  DBConnection.connect(function (err) {
    if (err) {
      console.error("error connecting: " + err.stack);
      return;
    }
    console.log("Successfully started connection to Database");
  });
  
};

main = async () => {
  initWebServer();
  initWebsocketServer();
  initMqttClient();

  startDBConnection();
  startWebServer();
  startWebSocketServer();
};

main().catch((error) => {
  // Possibility to globally react to errors.
  console.error(error);
  process.exit();
});
