const hostname = window.location.hostname;
const webSocketUrl = `ws://${hostname}:8999`;
const ws = new WebSocket(webSocketUrl);
var firstLoad = false;

const record = document.querySelector('#startRec');
const stop = document.querySelector('#endRec');
stop.disabled = true;
function date() {
  var d = new Date();
  var minutes = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
  var hours = d.getHours();
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  $("#date").html(
    `Date: ${day}.${month}.${year} <br> Time: ${hours}:${minutes}`
  );
}
setInterval(date, 1000);

// Called when receiving WebSocket message
ws.onmessage = ({ data }) => {
  if (data != "Picture") return;
  // Load new Picture and open PopUp
  var time = new Date().getTime();
  $("#img").html(`<img src="/cameraImg?t=${time}" class="img-fluid">`);
  $("#img").src = `/cameraImg?t=${time}`;
  $("#popUpHeader").text("New visitor:");
  $(".popup").show();
};

// Display the result of Server requests
function response(element, message, status) {
  element.text(message);
  switch (status) {
    case "success":
      element.css("color", "green");
      break;
    case "warning":
      element.css("color", "yellow");
      break;
    case "error":
      element.css("color", "red");
      break;
    default:
      break;
  }
}

window.onload = () => {
  // When the table is loaded get the currently selected Sound
  $("#soundTable").on("post-body.bs.table", function () {
    $.get("/getActiveSound", function (data, status) {
      $("#soundTable").bootstrapTable("checkBy", {
        // Check the currently selected Sound
        field: "fileName",
        values: [data],
      });
      firstLoad = true;
    });
  });
};

$(document).ready(function () {
  date();

  $(".popupCloseButton").click(function () {
    $(".popup").hide();
  });

  // Send Message to the server to signal that the door should be opened
  $("#openDoor").click(() => {
    console.log("open door");
    $.ajax({
      url: "/openDoor",
      type: "POST",
      dataType: "text",
      data: "Open door",
      async: true,
    });
  });

  // * Addresses

  // On Enter Press in the textbox, execute the click event
  $("#emailInput").on("keyup", (event) => {
    if (event.key !== "Enter") return;
    $("#submitEmail").click();
    event.preventDefault();
  });

  $("#submitEmail").click(function () {
    // Check if input is an email address
    if (
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(
        $("#emailInput").val()
      )
    ) {
      var emailInput = { address: $("#emailInput").val() };
      // Send input to server
      $.ajax({
        url: "/addAddress",
        type: "POST",
        dataType: "text",
        contentType: "application/json",
        data: JSON.stringify(emailInput),
        async: true,
        complete: function (jqXHR, status) {
          $("#addressTable").bootstrapTable("refresh");
          response($("#emailStatus"), jqXHR.responseText, status);
        },
      });
    } else {
      response(
        $("#emailStatus"),
        "The input has to be a valid email address",
        "error"
      );
    }
  });

  // * Sounds

  // Called when selecting a new sound
  // @param e necessary for this jquery event handler (not used)
  $("#soundTable").on("check.bs.table", function (e, row) {
    if (row.fileName == "Random")
      $("button[name='btnRemoveSound']").prop("disabled", true);
    else $("button[name='btnRemoveSound']").prop("disabled", false);
    if (!firstLoad) return;
    // Send the name of the selected file to the server
    $.ajax({
      url: "/setSound",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(row),
      complete: function (jqXHR, status) {
        response($("#soundStatus"), jqXHR.responseText, status);
      },
    });
  });

  // Called when a file is selected
  $("#newSound").on("change", () => {
    var form = document.forms.namedItem("newSound");
    var fd = new FormData(form);
    response($("#soundStatus"), "Uploading...", "success");
    // Send file to server
    $.ajax({
      url: "/newSound",
      type: "POST",
      dataType: "text",
      cache: false,
      contentType: false,
      processData: false,
      data: fd,
      complete: function (jqXHR, status) {
        $("#soundTable").bootstrapTable("refresh");
        response($("#soundStatus"), jqXHR.responseText, status);
      },
    });
  });

  if(navigator.mediaDevices.getUserMedia){
    console.log("getUserMedia is supported");
    const constraints = { audio: true };
    let chunks = [];
    
    let onSuccess = function(stream) {
      const mediaRecorder = new MediaRecorder(stream);
    
      record.onclick = function() {
        mediaRecorder.start();
        console.log(mediaRecorder.state);
        console.log("recorder started");
      
        stop.disabled = false;
        record.disabled = true;
      }
  
      stop.onclick = function() {
        mediaRecorder.stop();
        mediaRecorder.stream.getTracks().forEach((track) => track.stop())
        console.log(mediaRecorder.state);
        console.log("recorder stopped");
        record.style.background = "";
        record.style.color = "";
  
        stop.disabled = true;
        record.disabled = false;
      }
  
      mediaRecorder.onstop = function(e) {
        console.log("data available after MediaRecorder.stop() called.");
  
        const clipName = prompt('Enter a name for your sound clip?','My unnamed clip');

        const blob = new Blob(chunks, { 'type' : 'audio/wav; codecs=opus' });
        chunks = [];
        const blobUrl = URL.createObjectURL(blob);
        console.log(blobUrl);
        var fd = new FormData();
        fd.append("name", clipName)
        fd.append("soundUpload", blob, clipName);
        response($("#soundStatus"), "Uploading...", "success");
        // Send file to server
        $.ajax({
          url: "/newSound",
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          data: fd,
          complete: function (jqXHR, status) {
            $("#soundTable").bootstrapTable("refresh");
            response($("#soundStatus"), jqXHR.responseText, status);
          },
    });


        console.log("recorder stopped");
      }
  
      mediaRecorder.ondataavailable = function(e) {
        chunks.push(e.data);
      }
    }

    let onError = function(err) {
      console.log('The following error occured: ' + err);
      response($("#soundStatus"), `The following error occured: ${err}`, "error");
    }
    
    navigator.mediaDevices.getUserMedia({audio: constraints}).then(onSuccess, onError);


  } else {
    console.log("getUserMedia is not supported");
    record.disabled = true;
    response($("#soundStatus"), "getUserMedia is not supported", "error");
  }

  // * Unlock time

  // Get the current Unlock Time from the server and display it
  $.ajax({
    url: "/getUnlockTime",
    type: "GET",
    dataType: "text",
    complete: function (jqXHR) {
      $("#timeInput").val(jqXHR.responseText);
    },
  });

  // On Enter Press in the textbox, execute the click event
  $("#timeInput").on("keyup", (event) => {
    if (event.key !== "Enter") return;
    $("#submitTime").click();
    event.preventDefault();
  });

  $("#submitTime").on("click", () => {
    // Check if input is valid
    if (isNaN($("#timeInput").val())) {
      response($("#timeStatus"), "Time has to be a whole number", "error");
      return;
    }
    var inttime = parseInt($("#timeInput").val());
    var time = $("#timeInput").val();
    if (1 <= inttime && inttime <= 300) {
      // Send time to server
      $.ajax({
        url: "/setUnlockTime",
        type: "POST",
        contentType: "text/plain",
        data: time,
      });
    } else {
      response(
        $("#timeStatus"),
        "Time has to be a whole number between 1 and 300 seconds"
      );
    }
  });
});

// Creates the button to delete addresses and adds functionality to it
function removeAddress() {
  $table = $("#addressTable");
  return {
    btnRemoveAddress: {
      text: "Remove address",
      icon: "bi-trash-fill",

      event: function () {
        var selection = JSON.stringify($table.bootstrapTable("getSelections"));
        if ($table.bootstrapTable("getSelections").length == 0) return;
        $.ajax({
          url: "/removeAddress",
          type: "POST",
          dataType: "json",
          contentType: "application/json",
          data: selection,
          async: true,
          complete: function (jqXHR, status) {
            $("#addressTable").bootstrapTable("refresh");
            response($("#emailStatus"), jqXHR.responseText, status);
          },
        });
      },
    },
  };
}

// Creates the button to delete sounds and adds functionality to it
function removeSound() {
  return {
    btnRemoveSound: {
      text: "Remove sound",
      icon: "bi-trash-fill",
      // Called when button is pressed
      event: function () {
        var selection = JSON.stringify(
          $("#soundTable").bootstrapTable("getSelections")
        );
        // Check if something is checked
        if ($("#soundTable").bootstrapTable("getSelections").length == 0)
          return;
        // If Random is selected and button is pressed do nothing
        if (
          $("#soundTable").bootstrapTable("getSelections")[0].fileName ==
          "Random"
        )
          return;
        // Send the deleted soundname to the server
        $.ajax({
          url: "/removeSound",
          type: "POST",
          contentType: "application/json",
          data: selection,
          async: true,
          complete: function (jqXHR, status) {
            $("#soundTable").bootstrapTable("refresh");
            $("#soundTable").on("post-body.bs.table", function () {
              $("#soundTable").bootstrapTable("check", 0);
            });
            response($("#soundStatus"), jqXHR.responseText, status);
          },
        });
      },
    },
  };
}
