const hostname = window.location.hostname;
const webSocketUrl = `ws://${hostname}:8999`;
const ws = new WebSocket(webSocketUrl);

function date() {
  var d = new Date();
  var minutes = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
  var hours = d.getHours();
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  $("#date").html(
    `Date: ${day}.${month}.${year} <br> Time: ${hours}:${minutes}`
  );
}
setInterval(date, 1000);

// Called when receiving WebSocket message
ws.onmessage = ({ data }) => {
  if (data == "Ring") $("#historyTable").bootstrapTable("refresh");
  if (data == "Picure") {
    // Load new Picture and open PopUp
    var time = new Date().getTime();
    $("#img").html(`<img src="/cameraImg?t=${time}" class="img-fluid">`);
    $("#img").src = `/cameraImg?t=${time}`;
    $("#popUpHeader").text("Last visitor:");
    $('[name = "popUpOpenDoor"]').css("display", "block");
    $(".popup").show();
  }
};

$("document").ready(function () {
  date();

  // Open Popup and display the last picture
  $(".openPopup").click(function () {
    $("#img").html(`<img src="/cameraImg" class="img-fluid">`);
    $("#popUpHeader").text("Last visitor:");
    $('[name = "popUpOpenDoor"]').css("display", "none");
    $(".popup").show();
  });

  $(".popupCloseButton").click(function () {
    $(".popup").hide();
  });

  // Send Message to the server to signal that the door should be opened
  $("#openDoor").click(() => {
    console.log("open door");
    $.ajax({
      url: "/openDoor",
      type: "POST",
      dataType: "text",
      data: "Open door",
      async: true,
    });
  });
});
