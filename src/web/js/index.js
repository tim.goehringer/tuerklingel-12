const hostname = window.location.hostname;
const webSocketUrl = `ws://${hostname}:8999`;
const ws = new WebSocket(webSocketUrl);

// Displays the Date and Time on the UI
function date() {
  var d = new Date();
  var minutes = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
  var hours = d.getHours();
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  $("#date").html(
    `Date: ${day}.${month}.${year} <br> Time: ${hours}:${minutes}`
  );
}
setInterval(date, 1000);

// Called when receiving WebSocket message
ws.onmessage = ({ data }) => {
  switch (data) {
    case "Update":
      $("#statusTable").bootstrapTable("refresh");
      $("#statusTable").bootstrapTable("hideLoading");
      break;
    case "Ring":
      $("#statusTable").bootstrapTable("refresh");
      $("#statusTable").bootstrapTable("hideLoading");
      break;
    case "Picture":
      // Load new Picture and open PopUp
      var time = new Date().getTime();
      $("#img").html(`<img src="/cameraImg?t=${time}" class="img-fluid">`);
      $("#img").src = `/cameraImg?t=${time}`;
      $("#popUpHeader").text("New visitor:");
      $('[name = "popUpOpenDoor"]').css("display", "block");
      $(".popup").show();
      $("#statusTable").bootstrapTable("refresh");
      break;
  }
};

$(document).ready(function () {
  date();

  $("#statusTable").bootstrapTable("hideLoading");

  $(".openPopup").click(function () {
    // Open Popup and display the last picture
    $("#img").html(`<img src="/cameraImg" class="img-fluid">`);
    $("#popUpHeader").text("Last visitor:");
    $('[name = "popUpOpenDoor"]').css("display", "none");
    $(".popup").show();
  });

  $(".popupCloseButton").click(function () {
    $(".popup").hide();
  });

  // Get sysMode from the Serve and change the switch accordingly
  $.get("/getSysMode", function (data, status) {
    $("input[name='mode'][value=" + data + "]").prop("checked", true);
  });

  // When the Mode is changed, send the new Mode to the server
  $("#modeSelect").change(() => {
    selectedMode = { Mode: $("input[name='mode']:checked").val() };
    if (isNaN(selectedMode.Mode)) return;
    console.log(selectedMode);
    $.ajax({
      url: "/changeMode",
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      data: JSON.stringify(selectedMode),
      async: true,
    });
  });

  // Send Message to the server to signal that the door should be opened
  $("#openDoor").click(() => {
    console.log("open door");
    $.ajax({
      url: "/openDoor",
      type: "POST",
      dataType: "text",
      data: "Open door",
      async: true,
    });
  });
});
